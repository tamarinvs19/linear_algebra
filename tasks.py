import sys
from typing import Union

from src.expanders import run_zn_zn, run_zp
from src.graph_spectrum import NonIsomorphism
from src.iteration_methods import generate_start_vector, GaussSeidels, SimpleIterationMethod
from src.matrix import Matrix, Vector, HouseholderTransformationMatrix
from src.qr_eigenvalues import QREigenvalues
from src.qr_givens import QRDecompositionGivens, GivensRotationMatrix
from src.qr_householder import QRDecompositionHouseholder
from src.reader_writer import Reader, Writer
from src.simple_iteration_eigenvalue import SimpleIterationMaxEigenvalue
from src.three_diagonals import ThreeDiagonalization
from src.three_diagonals_eigenvalues import ThreeDiagonalsEigenvalues
from src.wilkinson_shift import WilkinsonShift


def task_1() -> None:
    matrix: Matrix = Reader.read_matrix()
    vector: Vector = Reader.read_vector()
    epsilon: float = Reader.read_float()
    ex_x0: Vector = generate_start_vector(vector.size)
    solution: Union[Vector, int] = SimpleIterationMethod(matrix, vector, epsilon).run(ex_x0)
    Writer.write_vector(solution)


def task_2() -> None:
    matrix: Matrix = Reader.read_matrix()
    vector: Vector = Reader.read_vector()
    epsilon: float = Reader.read_float()
    ex_x0: Vector = generate_start_vector(vector.size)
    solution: Union[Vector, int] = GaussSeidels(matrix, vector, epsilon).run(ex_x0)
    Writer.write_vector(solution)


def task_3() -> None:
    matrix: Matrix = Reader.read_matrix()
    i, j = Reader.read_pair_int('Введите номера строк')
    c, s = Reader.read_pair_float('Введите коэффициенты')
    matrix_g: GivensRotationMatrix = GivensRotationMatrix(matrix.width, i - 1, j - 1, s, c)
    Writer.write_matrix(matrix_g * matrix)


def task_4() -> None:
    matrix: Matrix = Reader.read_matrix()
    q, r = QRDecompositionGivens(matrix).run()
    Writer.write_matrix(q)
    Writer.write_matrix(r)


def task_5() -> None:
    matrix: Matrix = Reader.read_matrix()
    vector: Vector = Reader.read_vector()
    householder_matrix: HouseholderTransformationMatrix = HouseholderTransformationMatrix(vector)
    Writer.write_matrix(householder_matrix * matrix)


def task_6() -> None:
    matrix: Matrix = Reader.read_matrix()
    q, r = QRDecompositionHouseholder(matrix).run()
    Writer.write_matrix(q)
    Writer.write_matrix(r)


def task_7() -> None:
    matrix: Matrix = Reader.read_matrix()
    vector: Vector = Reader.read_vector()
    epsilon: float = Reader.read_float()
    eigenvalue, res_vector = SimpleIterationMaxEigenvalue(matrix, epsilon).run(vector)
    print(eigenvalue)
    Writer.write_vector(res_vector)


def task_8() -> None:
    matrix: Matrix = Reader.read_matrix('Введите симметрическую матрицу построчно, разделяя элементы пробелом')
    epsilon: float = Reader.read_float()
    eigenvalues, eigenvectors = QREigenvalues(matrix, epsilon).run()
    print(' '.join(map(str, eigenvalues)))
    Writer.write_matrix(eigenvectors)


def task_9() -> None:
    matrix: Matrix = Reader.read_matrix('Введите симметрическую матрицу построчно, разделяя элементы пробелом')
    three_diagonals, matrix_q = ThreeDiagonalization(matrix).run()
    Writer.write_matrix(three_diagonals)
    Writer.write_matrix(matrix_q)


def task_10() -> None:
    matrix: Matrix = Reader.read_matrix('Введите трехдиагональную симметрическую матрицу построчно, разделяя элементы '
                                        'пробелом')
    epsilon: float = Reader.read_float()
    eigenvalues, eigenvectors = ThreeDiagonalsEigenvalues(matrix, epsilon).run()
    print(' '.join(map(str, eigenvalues)))
    Writer.write_matrix(eigenvectors)


def task_11() -> None:
    matrix: Matrix = Reader.read_matrix('Введите трехдиагональную симметрическую матрицу построчно, разделяя элементы '
                                        'пробелом')
    epsilon: float = Reader.read_float()
    eigenvalues, eigenvectors = WilkinsonShift(matrix, epsilon).run()
    print(' '.join(map(str, eigenvalues)))
    Writer.write_matrix(eigenvectors)


def task_12() -> None:
    matrix_1: Matrix = Reader.read_matrix('Введите первую матрицу смежности')
    matrix_2: Matrix = Reader.read_matrix('Введите вторую матрицу смежности')
    print(NonIsomorphism(matrix_1, matrix_2).run())


def task_13() -> None:
    print('ZnxZn: {}'.format(run_zn_zn()))
    print('Zp: {}'.format(run_zp()))


if __name__ == '__main__':
    tasks = [
        task_1, task_2, task_3, task_4, task_5, task_6, task_7, task_8, task_9, task_10, task_11, task_12, task_13,
    ]
    task_number: int = int(sys.argv[1])
    tasks[task_number - 1]()
