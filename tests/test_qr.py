import unittest
from math import isclose

from src.matrix import Matrix, IdentityMatrix
from src.qr_givens import QRDecompositionGivens
from src.qr_householder import QRDecompositionHouseholder


def check_qr(matrix: Matrix, q: Matrix, r: Matrix) -> bool:
    qr: Matrix = q * r
    for i in range(matrix.height):
        for j in range(matrix.width):
            if not isclose(matrix.elements[i][j], qr.elements[i][j], abs_tol=0.000001):
                return False
    return True


class TestQRDecompositionGivens(unittest.TestCase):
    def test_identity_2x2(self) -> None:
        matrix: Matrix = IdentityMatrix(2)
        q, r = QRDecompositionGivens(matrix).run()
        self.assertTrue(check_qr(matrix, q, r))

    def test_identity_3x3(self) -> None:
        matrix: Matrix = IdentityMatrix(3)
        q, r = QRDecompositionGivens(matrix).run()
        self.assertTrue(check_qr(matrix, q, r))

    def test_identity_4x4(self) -> None:
        matrix: Matrix = IdentityMatrix(4)
        q, r = QRDecompositionGivens(matrix).run()
        self.assertTrue(check_qr(matrix, q, r))

    def test_2x2(self) -> None:
        matrix: Matrix = Matrix([[1, 2], [3, 4]])
        q, r = QRDecompositionGivens(matrix).run()
        self.assertTrue(check_qr(matrix, q, r))

    def test_3x3(self) -> None:
        matrix: Matrix = Matrix([[1, 2, 3], [2, 3, 9], [1, 5, 0]])
        q, r = QRDecompositionGivens(matrix).run()
        self.assertTrue(check_qr(matrix, q, r))

    def test_4x4(self) -> None:
        matrix: Matrix = Matrix([[1, 2, 3, 4], [2, 3, 9, 1], [1, 0, 5, 0], [2, 2, 8, 0]])
        q, r = QRDecompositionGivens(matrix).run()
        self.assertTrue(check_qr(matrix, q, r))


class TestQRDecompositionHouseholder(unittest.TestCase):
    def test_identity_2x2(self) -> None:
        matrix: Matrix = IdentityMatrix(2)
        q, r = QRDecompositionHouseholder(matrix).run()
        self.assertTrue(check_qr(matrix, q, r))

    def test_identity_3x3(self) -> None:
        matrix: Matrix = IdentityMatrix(3)
        q, r = QRDecompositionHouseholder(matrix).run()
        self.assertTrue(check_qr(matrix, q, r))

    def test_identity_4x4(self) -> None:
        matrix: Matrix = IdentityMatrix(4)
        q, r = QRDecompositionHouseholder(matrix).run()
        self.assertTrue(check_qr(matrix, q, r))

    def test_2x2(self) -> None:
        matrix: Matrix = Matrix([[1, 2], [3, 4]])
        q, r = QRDecompositionHouseholder(matrix).run()
        self.assertTrue(check_qr(matrix, q, r))

    def test_3x3(self) -> None:
        matrix: Matrix = Matrix([[1, 2, 3], [2, 3, 9], [1, 5, 0]])
        q, r = QRDecompositionHouseholder(matrix).run()
        self.assertTrue(check_qr(matrix, q, r))

    def test_4x4(self) -> None:
        matrix: Matrix = Matrix([[1, 2, 3, 4], [2, 3, 9, 1], [1, 0, 5, 0], [2, 2, 8, 0]])
        q, r = QRDecompositionHouseholder(matrix).run()
        self.assertTrue(check_qr(matrix, q, r))
