import unittest

from src.gershgorin_circles import Circle, GershgorinCircles
from src.matrix import Vector, Matrix


class TestCircle(unittest.TestCase):
    def test_init(self) -> None:
        circle: Circle = Circle(Vector([1, 2, 3]), 2)
        self.assertEqual(circle.coordinates.elements, Vector([1, 2, 3]).elements)
        self.assertEqual(circle.radius, 2)

    def test_contain_1(self) -> None:
        circle_first: Circle = Circle(Vector([1, 2, 3]), 2)
        circle_second: Circle = Circle(Vector([1.5, 2.4, 3.5]), 0.4)
        self.assertTrue(circle_second in circle_first)

    def test_contain_2(self) -> None:
        circle_first: Circle = Circle(Vector([1, 2, 3]), 2)
        circle_second: Circle = Circle(Vector([1.5, 2.4, 3.5]), 4)
        self.assertFalse(circle_second in circle_first)

    def test_contain_3(self) -> None:
        circle_first: Circle = Circle(Vector([1]), 2)
        circle_second: Circle = Circle(Vector([1.5]), 2)
        self.assertFalse(circle_second in circle_first)

    def test_contain_4(self) -> None:
        circle_first: Circle = Circle(Vector([1]), 2)
        circle_second: Circle = Circle(Vector([15]), 2)
        self.assertFalse(circle_second in circle_first)

    def test_contain_5(self) -> None:
        circle_first: Circle = Circle(Vector([1]), 20)
        circle_second: Circle = Circle(Vector([1]), 2)
        self.assertTrue(circle_second in circle_first)


class TestGershgorinCircles(unittest.TestCase):
    def test_calculate_circles(self) -> None:
        matrix: Matrix = Matrix([[1, 1, 1], [1, 1, 1], [1, 1, 1]])
        expected_circles = [Circle(Vector([1]), 6)] * 3
        self.assertEqual(
            GershgorinCircles.calculate_circles(matrix),
            expected_circles
        )

    def test_contain_in_unit_circle_1(self) -> None:
        circles = [Circle(Vector([0.1]), 0.5)] * 3
        self.assertTrue(
            GershgorinCircles.contain_in_unit_circle(circles)
        )

    def test_contain_in_unit_circle_2(self) -> None:
        circles = [Circle(Vector([0.1]), 5)] * 3
        self.assertFalse(
            GershgorinCircles.contain_in_unit_circle(circles)
        )
