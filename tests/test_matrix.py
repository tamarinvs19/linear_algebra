import unittest
from math import sqrt, isclose
from typing import List

from src.matrix import IdentityMatrix, Matrix, Vector


class TestIdentityMatrix(unittest.TestCase):
    def test_identity_matrix_one(self) -> None:
        matrix: IdentityMatrix = IdentityMatrix(1)
        self.assertEqual(matrix.size, 1)
        self.assertEqual(matrix.width, 1)
        self.assertEqual(matrix.height, 1)
        self.assertEqual(matrix.elements, [[1]])

    def test_identity_matrix_three(self) -> None:
        matrix: IdentityMatrix = IdentityMatrix(3)
        self.assertEqual(matrix.size, 3)
        self.assertEqual(matrix.width, 3)
        self.assertEqual(matrix.height, 3)
        self.assertEqual(matrix.elements, [[1, 0, 0], [0, 1, 0], [0, 0, 1]])

    def test_init_matrix_one(self) -> None:
        matrix: Matrix = Matrix([[239]])
        self.assertEqual(matrix.width, 1)
        self.assertEqual(matrix.height, 1)
        self.assertEqual(matrix.elements, [[239]])


class TestVector(unittest.TestCase):
    def test_init_vector(self) -> None:
        vector: Vector = Vector([1, 2, 3])
        self.assertEqual(vector.width, 1)
        self.assertEqual(vector.height, 3)
        self.assertEqual(vector.size, 3)
        self.assertEqual(vector.elements, [[1], [2], [3]])

    def test_vector_abs_1(self) -> None:
        vector: Vector = Vector([1, 2, 3])
        self.assertEqual(isclose(abs(vector), sqrt(14.0), abs_tol=10**-15), True)

    def test_vector_abs_2(self) -> None:
        vector: Vector = Vector([-0.5])
        self.assertEqual(abs(vector), 0.5)

    def test_vector_str(self) -> None:
        vector: Vector = Vector([1, 2, 3])
        self.assertEqual(str(vector), "(1, 2, 3)^T")

    def test_vector_add(self) -> None:
        vector_first: Vector = Vector([1, 2, 3])
        vector_second: Vector = Vector([4, 5, 6])
        vector_sum: Vector = vector_first + vector_second
        vector_sum_reverse: Vector = vector_second + vector_first
        self.assertEqual(vector_sum.elements, [[5], [7], [9]])
        self.assertEqual(vector_sum_reverse.elements, [[5], [7], [9]])

    def test_vector_mul(self) -> None:
        vector_first: Vector = Vector([1, 2, 3])
        number: float = 10.0
        vector_mul: Vector = vector_first * number
        self.assertEqual(vector_mul.elements, [[10], [20], [30]])

    def test_vector_add_error(self) -> None:
        vector_first: Vector = Vector([1, 2, 3])
        vector_second: Vector = Vector([10, 20])
        self.assertRaises(ValueError, vector_first.__add__, vector_second)

    def test_vector_sub_error(self) -> None:
        vector_first: Vector = Vector([1, 2, 3])
        vector_second: Vector = Vector([10, 20])
        self.assertRaises(ValueError, vector_first.__sub__, vector_second)

    def test_vector_sub(self) -> None:
        vector_first: Vector = Vector([1, 2, 3])
        vector_second: Vector = Vector([10, 20, 30])
        vector_sub: Vector = vector_first - vector_second
        self.assertEqual(vector_sub.elements, [[-9], [-18], [-27]])

    def test_transpose(self) -> None:
        vector: Vector = Vector([1, 2, 3])
        transposed: Matrix = Matrix([[1, 2, 3]])
        self.assertEqual(vector.transpose().elements, transposed.elements)


class TestMatrix(unittest.TestCase):
    def test_diagonal_elements_1(self) -> None:
        matrix: Matrix = Matrix([[1, 2, 3], [4, 5, 6]])
        diagonal_elements: List[float] = matrix.diagonal_elements
        self.assertEqual(diagonal_elements, [1, 5])

    def test_diagonal_elements_2(self) -> None:
        matrix: Matrix = Matrix([[1, 2], [4, 5]])
        diagonal_elements: List[float] = matrix.diagonal_elements
        self.assertEqual(diagonal_elements, [1, 5])

    def test_diagonal_elements_3(self) -> None:
        matrix: Matrix = Matrix([[1], [4]])
        diagonal_elements: List[float] = matrix.diagonal_elements
        self.assertEqual(diagonal_elements, [1])

    def test_to_vector_error(self) -> None:
        matrix: Matrix = Matrix([[1, 2], [4, 5]])
        self.assertRaises(TypeError, matrix.to_vector)

    def test_to_vector(self) -> None:
        matrix: Matrix = Matrix([[1], [2], [3]])
        self.assertEqual(matrix.to_vector().elements, [[1], [2], [3]])
        self.assertTrue(isinstance(matrix.to_vector(), Vector))

    def test_init_matrix_two_three(self) -> None:
        matrix: Matrix = Matrix([[1, 2, 3], [4, 5, 6]])
        self.assertEqual(matrix.width, 3)
        self.assertEqual(matrix.height, 2)
        self.assertEqual(matrix.elements, [[1, 2, 3], [4, 5, 6]])

    def test_matrix_add(self) -> None:
        matrix_first: Matrix = Matrix([[1, 2, 3], [4, 5, 6]])
        matrix_second: Matrix = Matrix([[100, 200, 300], [400, 500, 600]])
        matrix_sum: Matrix = matrix_first + matrix_second
        matrix_sum_reverse: Matrix = matrix_second + matrix_first
        self.assertEqual(matrix_sum.elements, [[101, 202, 303], [404, 505, 606]])
        self.assertEqual(matrix_sum_reverse.elements, [[101, 202, 303], [404, 505, 606]])

    def test_matrix_mul_1(self) -> None:
        matrix_first: Matrix = Matrix([[1, 2, 3], [4, 5, 6]])
        matrix_second: Matrix = Matrix([[10], [20], [30]])
        matrix_mul: Matrix = matrix_first * matrix_second
        self.assertEqual(matrix_mul.elements, [[10 + 40 + 90], [40 + 100 + 180]])

    def test_matrix_mul_2(self) -> None:
        matrix_first: Matrix = Matrix([[1, 2], [3, 4], [5, 6]])
        matrix_second: Matrix = Matrix([[10, 20], [30, 40]])
        matrix_mul: Matrix = matrix_first * matrix_second
        self.assertEqual(matrix_mul.elements, [[10 + 60, 20 + 80], [30 + 120, 60 + 160], [50 + 180, 100 + 240]])

    def test_matrix_mul_3_size_error(self) -> None:
        matrix_first: Matrix = Matrix([[1, 2, 3], [4, 5, 6]])
        matrix_second: Matrix = Matrix([[10, 20], [30, 40]])
        self.assertRaises(ValueError, matrix_first.__mul__, matrix_second)

    def test_matrix_mul_4_type_error(self) -> None:
        matrix: Matrix = Matrix([[1, 2, 3], [4, 5, 6]])
        self.assertRaises(TypeError, matrix.__mul__, 'random')

    def test_matrix_mul_5_number(self) -> None:
        matrix_first: Matrix = Matrix([[1, 2], [3, 4], [5, 6]])
        number: float = 5.0
        matrix_mul: Matrix = matrix_first * number
        self.assertEqual(matrix_mul.elements, [[5, 10], [15, 20], [25, 30]])

    def test_is_symmetric_true_1(self) -> None:
        matrix: Matrix = Matrix([[1, 2, 3], [2, 5, 6], [3, 6, 9]])
        self.assertTrue(matrix.is_symmetric)

    def test_is_symmetric_true_2(self) -> None:
        matrix: Matrix = Matrix([[1, 2], [2, 5]])
        self.assertTrue(matrix.is_symmetric)

    def test_is_symmetric_false_1(self) -> None:
        matrix: Matrix = Matrix([[1, 7, 3], [2, 5, 6], [3, 6, 9]])
        self.assertFalse(matrix.is_symmetric)

    def test_is_symmetric_false_2(self) -> None:
        matrix: Matrix = Matrix([[1, 7], [2, 5], [3, 6]])
        self.assertFalse(matrix.is_symmetric)
