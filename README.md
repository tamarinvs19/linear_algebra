# Вычислительная линейная алгебра
## Запуск
Каждую задачу можно запустить из командной строки с помощью следующей команды из корня проекта:
```bash
python3 tasks.py <номер задачи>
```

Ввод осуществляется из консоли согласно условию задания. Парсинг ввода можно посмотреть в файле `tasks.py`.
Предполагается, что матрицы квадратные, поэтому их размер при вводе первой строки определяется автоматически.

В __третьей__ задаче `i, j` и `s, c` вводятся в парах по строке на каждую.

Для работы с матрицами используется класс `Matrix` (`src/matrix.py`), а так же `IdentityMatrix` (для единичной матрицы), `Vector` и `BlockMatrix` (блочная матрица их четырех элементов). 

## Итерационные методы решения систем линейных уравнений

### Метод простой итерации
При реализации использованы круги Гершгорина (`src/gershgorin_circles.py`).
#### Задача 1  (`src/iteration_methods.py`) 
```python
from src.matrix import Matrix, Vector
from src.iteration_methods import SimpleIterationMethod
ex_A: Matrix = Matrix([[0.9, 0.3], [0.3, 0.8]])
ex_b: Vector = Vector([0.43, 0.52])
ex_epsilon: float = 10**-5
ex_x0: Vector = Vector([1, 1])
solutionSIM: Vector = SimpleIterationMethod(ex_A, ex_b, ex_epsilon).run(ex_x0)
```

### Метод Гаусса-Зейделя
#### Задача 2 (`src/iteration_methods.py`)
```python
from src.matrix import Matrix, Vector
from src.iteration_methods import generate_start_vector, GaussSeidels
ex_A: Matrix = Matrix([[0.9, 0.3], [0.3, 0.8]])
ex_b: Vector = Vector([0.43, 0.52])
ex_epsilon: float = 0.00001
ex_x0: Vector = generate_start_vector(2)
solutionGS: Vector = GaussSeidels(ex_A, ex_b, ex_epsilon).run(ex_x0)
```

## QR-разложение
### Вращения Гивенса
#### Задача 3 (`src/qr_givens.py`)
Класс `GivensRotationMatrix` инициализируется размером матрицы, индексами строк и значениями синуса и косинуса угла поворота.

Для него переопределено левое умножение (`G(i, j, phi) * A`), работающее за линейное от размера время.

#### Задача 4 (`src/qr_givens.py`)
Метод `run()` класса `QRDecompositionGivens` возвращает две матрицы Q и R.
```python
from src.matrix import Matrix
from src.qr_givens import QRDecompositionGivens
ex_matrix: Matrix = Matrix([
        [0,  2,  3,  4,  5,  6],
        [2,  8,  9,  10, 11, 12],
        [3, 9, 0, 16, 17, 18],
        [9, 0, 16, 22, 23, 24],
        [5, 11, 17, 23, 29, 30],
        [6, 12, 18, 24, 30, 36],
    ])
q, r = QRDecompositionGivens(ex_matrix).run()
```

### Отражения Хаусхолдера
#### Задача 5 (`src/matrix.py`)
Используется класс `HouseholderTransformationMatrix` (`src/matrix.py`), который хранит вектор и поддерживает левое и правое быстрое умножение на квадратную матрицу.

#### Задача 6 (`src/qr_householder.py`)
Класс `QRDecompositionHouseholder` инициализируется матрицей, метод `run()` запускает процесс разложения и возвращает пару матриц Q и R.
```python
from src.matrix import Matrix
from src.qr_householder import QRDecompositionHouseholder
ex_matrix: Matrix = Matrix([
        [0,  2,  3,  4,  5,  6],
        [2,  9,  9,  10, 11, 12],
        [3, 9, 0, 16, 17, 18],
        [9, 0, 16, 2, 3, 24],
        [5, 11, 17, 3, 29, 30],
        [6, 12, 18, 24, 30, 36],
])
q, r = QRDecompositionHouseholder(ex_matrix).run()
```

## Алгоритмы нахождения собственных чисел
### Простая итерация
#### Задача 7 (`src/simple_iteration_eigenvalue.py`)
Класс `SimpleIterationMaxEigenvalue` инициализируется матрицей и погрешностью `epsilon`. Метод `run(vector: Vector)` принимает стартовый вектор и запускает процесс вычисления максимального собственного числа. 

Можно сгенерировать случайный стартовый вектор с помощью `SimpleIterationMaxEigenvalue.generate_start_vector(size: int)`: принимает длину вектора, возвращает случайный вектор единичной длины.
```python
from src.matrix import Matrix, Vector
from src.simple_iteration_eigenvalue import SimpleIterationMaxEigenvalue
ex_matrix: Matrix = Matrix([[4, 1], [2, -1]])
ex_vector: Vector = SimpleIterationMaxEigenvalue.generate_start_vector(ex_matrix.width)
ex_epsilon: float = 10**-8
result = SimpleIterationMaxEigenvalue(ex_matrix, ex_epsilon).run(ex_vector)
```

### QR-алгоритм
#### Задача 8 (`src/qr_eigenvalues.py`)
Класс `QREigenvalues` инициализируется симметричной матрицей и погрешностью `epsilon`. Можно использовать дополнительный параметр `approx: bool`, если в исходной матрице допустима симметрия с погрешностью. 

Если матрица не является симметричной, выбрасывается исключение `ValueError`. 

Метод `run()` возвращает пару полученных собственных чисел и матрицы, столбцы которой -- собственные вектора.
```python
from src.matrix import Matrix
from src.qr_eigenvalues import QREigenvalues
ex_matrix: Matrix = Matrix([
        [1, 1, 0, 0, 2],
        [1, 2, 1, 9, 0],
        [0, 1, 1, 3, 0],
        [0, 9, 3, 3, 4],
        [2, 0, 0, 4, 2]
])
ex_epsilon: float = 10**-4
ex_eigenvalues, ex_eigenvectors = QREigenvalues(ex_matrix, ex_epsilon).run()
```

### Трехдиагональные матрицы
#### Задача 9 (`src/three_diagonals.py`)
Класс `ThreeDiagonalization` инициализируется симметричной матрицей, если матрица не является симметричной, выбрасывается исключение `ValueError`. Метод `run()` запускает вычисление и возвращает пару из диагонализированной матрицы и матрицы Q. 
```python
from src.matrix import Matrix
from src.three_diagonals import ThreeDiagonalization
ex_matrix: Matrix = Matrix([
        [1,  2,  3,  4,  5,  6],
        [2,  8,  9,  10, 11, 12],
        [3, 9, 15, 16, 17, 18],
        [4, 10, 16, 22, 23, 24],
        [5, 11, 17, 23, 29, 30],
        [6, 12, 18, 24, 30, 36],
])
ex_three_diagonals, ex_matrix_q = ThreeDiagonalization(ex_matrix).run()
```

### Применение в QR-алгоритме
#### Задача 10 (`src/three_diagonals_eigenvalues.py`)
Класс `QRThreeDiagonalsDecompositionGivens` раскладывает трехдиагональную матрицу в произведение Q и R, используя вращения Гивенса.

Класс `ThreeDiagonalsEigenvalues` инициализируется матрицей и погрешностью `epsilon`. Метод `run()` возвращает собственные числа и матрицу Q, когда радиусы кругов Гершгорина стали меньше `epsilon`.
```python
from src.matrix import Matrix
from src.three_diagonals_eigenvalues import ThreeDiagonalsEigenvalues
ex_matrix: Matrix = Matrix([
        [1, 1, 0, 0, 0],
        [1, 2, 1, 0, 0],
        [0, 1, 1, 3, 0],
        [0, 0, 3, 3, 4],
        [0, 0, 0, 4, 2]
])
ex_epsilon: float = 10**-7
ex_eigenvalues, ex_eigenvectors = ThreeDiagonalsEigenvalues(ex_matrix, ex_epsilon).run()
```

### Сдвиг
#### Задача 11 (`src/wilkinson_shift.py`)
Класс `WilkinsonShift` инициализируется матрицей и погрешностью.
```python
from src.matrix import Matrix
from src.wilkinson_shift import WilkinsonShift
ex_matrix: Matrix = Matrix([
        [1, 1, 0, 0, 0],
        [1, 2, 1, 0, 0],
        [0, 1, 1, 3, 0],
        [0, 0, 3, 3, 4],
        [0, 0, 0, 4, 2]
])
ex_epsilon: float = 10**-8
ex_eigenvalues, ex_eigenvectors = WilkinsonShift(ex_matrix, ex_epsilon).run()
```

## Спектры графов
### Неизоморфность
#### Задание 12 (`src/spectrum_graph.py`)
Класс `NonIsomorphism` инициализируется двумя матрицами смежности, метод `run()` возвращает `True`, если графы точно не могут быть изоморфны и `False` иначе.
```python
from src.matrix import Matrix
from src.graph_spectrum import NonIsomorphism
ex_matrix1: Matrix = Matrix([
        [1, 1, 0, 0, 0],
        [1, 2, 1, 0, 0],
        [0, 1, 1, 3, 0],
        [0, 0, 3, 3, 4],
        [0, 0, 0, 4, 2]
])
ex_matrix2: Matrix = Matrix([
        [1, 1, 0, 0, 0],
        [1, 2, 1, 0, 0],
        [0, 1, 1, 3, 0],
        [0, 0, 3, 3, 4],
        [0, 0, 0, 4, 2]
])
ex_matrix3: Matrix = Matrix([
        [0, 1, 0, 0, 1],
        [1, 2, 1, 0, 0],
        [0, 1, 1, 3, 0],
        [0, 0, 3, 3, 4],
        [1, 0, 0, 4, 2]
])
# вернет False, так как эти графы изоморфны
result12: bool = NonIsomorphism(ex_matrix1, ex_matrix2).run()
# вернет True, так как эти графы неизоморфны
result13: bool = NonIsomorphism(ex_matrix1, ex_matrix3).run()
```

### Экспандеры
#### Задача 13 (`src/expanders.py`)
Для обоих графов написаны классы: `ExpanderZnZn` и `ExpanderZp`. Инициализация по размеру графа. Метод `run()` возвращает максимальный из модулей второго и последнего собственных чисел, деленный на степень вершины данного графа.

