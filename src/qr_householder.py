import copy
from typing import Tuple

from src.matrix import Matrix, Vector, IdentityMatrix, BlockMatrix, HouseholderTransformationMatrix


class QRDecompositionHouseholder(object):
    """
    Usage:
    $> matrix = Matrix([1, 2], [3, 4])
    $> q, r = QRDecompositionHouseholder(matrix).run()
    """
    def __init__(self, matrix: Matrix) -> None:
        self.matrix = matrix
        self.size = matrix.height

    def run(self) -> Tuple[Matrix, Matrix]:
        qk_transposed: Matrix = IdentityMatrix(self.size)
        ak: Matrix = copy.deepcopy(self.matrix)

        for size in range(self.size - 1):
            hh_size: int = self.size - size
            e: Vector = Vector([1] + [0] * (hh_size - 1))
            v: Vector = Vector([line[size] for line in ak.elements[size:]])
            u: Vector = v * (1 / abs(v))
            if abs(u - v) != 0:
                transform: Vector = (u - e) * (1 / abs(u - e))
                if size == 0:
                    householder_matrix = HouseholderTransformationMatrix(transform)
                    ak = householder_matrix * ak
                    qk_transposed = householder_matrix * qk_transposed
                else:
                    householder_matrix = BlockMatrix(
                        (
                            (IdentityMatrix(size), Matrix([[0]*hh_size for _ in range(size)])),
                            (Matrix([[0]*size for _ in range(hh_size)]), HouseholderTransformationMatrix(transform))
                        )
                    )
                    ak = (householder_matrix * ak.to_block_matrix(size, size)).to_matrix()
                    qk_transposed = (householder_matrix * qk_transposed.to_block_matrix(size, size)).to_matrix()
        return qk_transposed.transpose(), ak


if __name__ == "__main__":
    ex_matrix: Matrix = Matrix([
        [0,  2,  3,  4,  5,  6],
        [2,  9,  9,  10, 11, 12],
        [3, 9, 0, 16, 17, 18],
        [9, 0, 16, 2, 3, 24],
        [5, 11, 17, 3, 29, 30],
        [6, 12, 18, 24, 30, 36],
    ])
    q, r = QRDecompositionHouseholder(ex_matrix).run()
    print('Q: \n{}'.format(q))
    print('R: \n{}'.format(r))
    print('QR: \n{}'.format(q*r))
