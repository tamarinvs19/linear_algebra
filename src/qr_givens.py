import itertools
from math import isclose, sqrt
from typing import List, Union, Tuple

from src.matrix import Matrix, IdentityMatrix, Vector, Number


class GivensRotationMatrix(Matrix):
    """Матрица вращения Гивенса"""
    def __init__(self, size: int, i: int, j: int, sin: float, cos: float):
        """
        size: int = размер матрицы
        i: int, j: int = номера строк/столбцов
        sin: float, cos: float = синус и косинус угла вращения
        """
        if not 0 <= i < j < size:
            raise ValueError("Incorrect arguments: i, j")
        if not isclose(sin**2 + cos**2, 1, abs_tol=10**(-6)):
            raise ValueError("Incorrect arguments: sin, cos")

        self.cos: float = cos
        self.sin: float = sin
        self.size: int = size
        self.i: int = i
        self.j: int = j

        elements: List[List[float]] = IdentityMatrix(size).elements
        elements[i][i] = self.cos
        elements[j][j] = self.cos
        elements[i][j] = self.sin
        elements[j][i] = -self.sin
        super().__init__(elements)

    def __mul__(self, other: Union[Matrix, Vector]) -> Union[Matrix, Vector]:
        """Левое умножение данной матрицы на другую матрицу или вектор"""
        def update_elements(line: int) -> Vector:
            if line == self.i:
                return Vector(other.elements[self.i]) * self.cos + \
                       Vector(other.elements[self.j]) * self.sin
            elif line == self.j:
                return Vector(other.elements[self.i]) * (-self.sin) + \
                       Vector(other.elements[self.j]) * self.cos
            else:
                return Vector(other.elements[line])

        if self.size != other.height:
            raise ArithmeticError("Matrix have a different sizes")
        if isinstance(other, Vector):
            return Matrix([
                update_elements(i).to_list()
                for i in range(self.size)
            ]).to_vector()
        elif isinstance(other, Matrix):
            return Matrix([
                update_elements(i).to_list()
                for i in range(self.size)
            ])
        else:
            raise TypeError("It is not a Matrix or Vector")


class QRDecompositionGivens(object):
    """
    Usage:
    $> matrix = Matrix([1, 2], [3, 4])
    $> q, r = QRDecompositionGivens(matrix).run()
    """
    def __init__(self, matrix: Matrix) -> None:
        self.matrix: Matrix = matrix
        self.size: int = self.matrix.height
        self.qk_transposed: Matrix = IdentityMatrix(self.size)

    def givens_multiplication(self, matrix: Matrix, i: int, j: int, sin: float, cos: float) -> Matrix:
        givens_matrix: GivensRotationMatrix = GivensRotationMatrix(self.matrix.height, i, j, sin, cos)
        return givens_matrix * matrix

    def run(self) -> Tuple[Matrix, Matrix]:
        for i, j in itertools.combinations_with_replacement(range(self.size), 2):
            if i == j:
                continue
            x: Number = self.matrix.elements[i][i]
            y: Number = self.matrix.elements[j][i]
            if y != 0:
                x1: Number = x / y
                sgn_x: int = 1 if x >= 0 else -1
                s: float = sgn_x / (sqrt(1 + x1 ** 2))
                c: float = s * x1
                givens_matrix: GivensRotationMatrix = GivensRotationMatrix(self.matrix.height, i, j, s, c)
                self.matrix = givens_matrix * self.matrix
                self.qk_transposed = givens_matrix * self.qk_transposed
            else:
                givens_matrix: GivensRotationMatrix = GivensRotationMatrix(self.matrix.height, i, j, 0, 1)
                self.matrix = givens_matrix * self.matrix
                self.qk_transposed = givens_matrix * self.qk_transposed

        return self.qk_transposed.transpose(), self.matrix


if __name__ == '__main__':
    ex_matrix: Matrix = Matrix([
        [0,  2,  3,  4,  5,  6],
        [2,  8,  9,  10, 11, 12],
        [3, 9, 0, 16, 17, 18],
        [9, 0, 16, 22, 23, 24],
        [5, 11, 17, 23, 29, 30],
        [6, 12, 18, 24, 30, 36],
    ])
    q, r = QRDecompositionGivens(ex_matrix).run()
    print('Q: \n{}'.format(q))
    print('R: \n{}'.format(r))
    print('QR: \n{}'.format(q*r))
