from math import isclose
from typing import List

from src.matrix import Matrix, Vector
from src.three_diagonals import ThreeDiagonalization
from src.three_diagonals_eigenvalues import ThreeDiagonalsEigenvalues


class NonIsomorphism(object):
    """
    Usage:
    result: bool = NonIsomorphism(ex_matrix1, ex_matrix2).run()
    """
    def __init__(self, matrix1: Matrix, matrix2: Matrix) -> None:
        self.matrix1: Matrix = matrix1
        self.matrix2: Matrix = matrix2
        self.epsilon: float = 10**-7

    def run(self) -> bool:
        three_diagonals1, q1 = ThreeDiagonalization(self.matrix1).run()
        eigenvalues1, eigenvectors_matrix1 = ThreeDiagonalsEigenvalues(three_diagonals1, self.epsilon).run()
        eigenvalues1 = [q_diag * value for q_diag, value in
                        zip(q1.diagonal_elements, eigenvalues1)]
        eigenvectors_matrix1 = q1 * eigenvectors_matrix1

        three_diagonals2, q2 = ThreeDiagonalization(self.matrix2).run()
        eigenvalues2, eigenvectors_matrix2 = ThreeDiagonalsEigenvalues(three_diagonals2, self.epsilon).run()
        eigenvalues2 = [q_diag * value for q_diag, value in
                        zip(q2.diagonal_elements, eigenvalues2)]
        eigenvectors_matrix2 = q2 * eigenvectors_matrix2

        eigenvectors1: List[Vector] = eigenvectors_matrix1.to_vectors()
        eigenvectors2: List[Vector] = eigenvectors_matrix2.to_vectors()
        eigenvectors1.sort(key=lambda x: abs(x))
        eigenvectors2.sort(key=lambda x: abs(x))

        compare_values = all(
            isclose(e1, e2, abs_tol=self.epsilon)
            for e1, e2 in zip(eigenvalues1, eigenvalues2)
        )

        compare_vectors = all(
            all(isclose(v1.elements[i][0], v2.elements[i][0], abs_tol=self.epsilon) for i in range(v1.size))
            for v1, v2 in zip(eigenvectors1, eigenvectors2)
        )

        return not (compare_values and compare_vectors)


if __name__ == '__main__':
    ex_matrix1: Matrix = Matrix([
        [1, 1, 0, 0, 0],
        [1, 2, 1, 0, 0],
        [0, 1, 1, 3, 0],
        [0, 0, 3, 3, 4],
        [0, 0, 0, 4, 2]
    ])
    ex_matrix2: Matrix = Matrix([
        [1, 1, 0, 0, 0],
        [1, 2, 1, 0, 0],
        [0, 1, 1, 3, 0],
        [0, 0, 3, 3, 4],
        [0, 0, 0, 4, 2]
    ])
    ex_matrix3: Matrix = Matrix([
        [0, 1, 0, 0, 1],
        [1, 2, 1, 0, 0],
        [0, 1, 1, 3, 0],
        [0, 0, 3, 3, 4],
        [1, 0, 0, 4, 2]
    ])
    result12: bool = NonIsomorphism(ex_matrix1, ex_matrix2).run()
    result13: bool = NonIsomorphism(ex_matrix1, ex_matrix3).run()
    print('G1 non G2: {}'.format(result12))
    print('G1 non G3: {}'.format(result13))
