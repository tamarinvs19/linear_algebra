from typing import List, Tuple

from src.matrix import Matrix, Number, IdentityMatrix
from src.qr_eigenvalues import QREigenvalues
from src.three_diagonals_eigenvalues import QRThreeDiagonalsDecompositionGivens


class WilkinsonShift(object):
    """
    Usage:
    $> ex_matrix: Matrix = Matrix([
        [1, 1, 0, 0, 0],
        [1, 2, 1, 0, 0],
        [0, 1, 1, 3, 0],
        [0, 0, 3, 3, 4],
        [0, 0, 0, 4, 2]
    ])
    $> ex_epsilon: float = 10**-8
    $> ex_eigenvalues, ex_eigenvectors = WilkinsonShift(ex_matrix, ex_epsilon).run()
    """
    def __init__(self, matrix: Matrix, epsilon: float) -> None:
        self.matrix: Matrix = matrix
        self.size = self.matrix.width
        self.epsilon: float = epsilon
        self.eigenvalues: List[Number] = []
        self.qk_transposed: Matrix = IdentityMatrix(self.size)

    def start_approximation(self, index: int) -> Number:
        """
        Вычисляет начальное приближение для подматрицы 2x2 с левым вершим углом с координатами index, index
        """
        submatrix: Matrix = Matrix([
            [self.matrix.elements[index-1][index-1], self.matrix.elements[index-1][index]],
            [self.matrix.elements[index][index-1], self.matrix.elements[index][index]]
        ])
        print(submatrix)
        eigenvalues: List[Number] = QREigenvalues(submatrix, 0.1, True).run()[0]
        if abs(self.matrix.elements[index][index] - eigenvalues[0]) < \
                abs(self.matrix.elements[index][index] - eigenvalues[1]):
            return eigenvalues[0]
        else:
            return eigenvalues[1]

    def check_approx_zero(self, index: int) -> bool:
        """Проверяет,что все элементы кроме диагонального в i-ой строке и i-ом столбце близки к нулю"""
        index_pairs: List[Tuple[int, int]] = \
            [(index, j) for j in range(self.size) if j != index] + \
            [(j, index) for j in range(self.size) if j != index]
        return all(abs(self.matrix.elements[i][j]) < self.epsilon for i, j in index_pairs)

    def run(self) -> Tuple[List[Number], Matrix]:
        for index in range(self.size - 1, -1, -1):
            s: Number = self.start_approximation(index)
            matrix_i: Matrix = Matrix([
                [1 if i == j and i <= index else 0 for j in range(self.size)]
                for i in range(self.size)
            ])
            while not self.check_approx_zero(index):
                matrix_q, matrix_r = QRThreeDiagonalsDecompositionGivens(self.matrix + matrix_i * (-s)).run()
                self.matrix = matrix_r * matrix_q + matrix_i * s
                self.qk_transposed *= matrix_q
            self.eigenvalues.append(self.matrix.elements[index][index])
        return self.eigenvalues, self.qk_transposed.transpose()


if __name__ == '__main__':
    ex_matrix: Matrix = Matrix([
        [1, 1, 0, 0, 0],
        [1, 2, 1, 0, 0],
        [0, 1, 1, 3, 0],
        [0, 0, 3, 3, 4],
        [0, 0, 0, 4, 2]
    ])
    ex_epsilon: float = 10**-8
    ex_eigenvalues, ex_eigenvectors = WilkinsonShift(ex_matrix, ex_epsilon).run()
    print('Eigenvalues:\n{}'.format(ex_eigenvalues))
    print('Eigenvectors:\n{}'.format(ex_eigenvectors))
