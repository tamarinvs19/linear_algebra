from __future__ import annotations

from itertools import product
from typing import List

from src.matrix import Matrix, Vector


class Circle(object):
    """
    Круг. Задается радиусом и радиус-вектором центра круга. Работает для любой размерности.
    Поддерживает операции сравнения на равенство и на вложенность.
    """
    def __init__(self, coordinates: Vector, radius: float):
        self.coordinates = coordinates
        self.radius = radius

    def __contains__(self, item: Circle) -> bool:
        return abs(self.coordinates - item.coordinates) <= self.radius - item.radius

    def __eq__(self, other) -> bool:
        return \
            self.coordinates == other.coordinates and\
            self.radius == other.radius

    def __repr__(self) -> str:
        return 'Circle: {}, radius={}'.format(self.coordinates, self.radius)


class GershgorinCircles(object):
    """
    Содержит два метода для работы с кругами Гершгорина
    """
    @staticmethod
    def calculate_circles(matrix: Matrix) -> List[Circle]:
        """Вычисляет для матрицы круги Гершгорина"""
        sum_not_diagonal = sum(
            abs(matrix.elements[i][j])
            for (i, j) in product(
                range(matrix.height),
                range(matrix.width)
            )
            if i != j
        )
        return [
            Circle(Vector([element]), sum_not_diagonal)
            for element in matrix.diagonal_elements
        ]

    @staticmethod
    def contain_in_unit_circle(circles: List[Circle]) -> bool:
        """
        Поверяет, что каждый круг из полученного списка содержится в единичном круге.
        Все круги должны иметь одинаковую размерность, иначе бросается исключение ValueError.
        """
        dimension: int = circles[0].coordinates.size
        if all(circle.coordinates.size == dimension for circle in circles):
            unit_circle: Circle = Circle(Vector([0]*dimension), 1)
            return all(circle in unit_circle for circle in circles)
        else:
            raise ValueError("Different dimensions")
