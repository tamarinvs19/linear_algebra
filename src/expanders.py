from abc import ABC, abstractmethod
from math import sqrt
from typing import Tuple, List

from src.matrix import Matrix, Number
from src.three_diagonals import ThreeDiagonalization
from src.three_diagonals_eigenvalues import ThreeDiagonalsEigenvalues


class AbstractExpander(ABC):
    def __init__(self, graph: Matrix, degree: int, epsilon: float) -> None:
        self.graph: Matrix = graph
        self.epsilon: float = epsilon
        self.degree: int = degree
        self.build_graph()

    @abstractmethod
    def build_graph(self) -> None:
        pass

    def run(self) -> Number:
        self.build_graph()
        result_diagonalization: Tuple[Matrix, Matrix] = ThreeDiagonalization(self.graph).run()
        diagonalization: Matrix = result_diagonalization[0]
        eigenvalues: List[Number] = ThreeDiagonalsEigenvalues(diagonalization, self.epsilon).run()[0]
        eigenvalues = [q_diag * value
                       for q_diag, value in zip(result_diagonalization[1].diagonal_elements, eigenvalues)]
        eigenvalues.sort(reverse=True)
        return max(abs(eigenvalues[1]), abs(eigenvalues[-1])) / self.degree


class ExpanderZnZn(AbstractExpander):
    def __init__(self, size: int) -> None:
        self.size: int = size
        super().__init__(Matrix([[0] * size**2 for _ in range(size**2)]), 8, 10**-5)

    def edges(self, x: int, y: int) -> List[Tuple[int, int]]:
        def mod(a): return (3 * self.size + a) % self.size

        return list(map(lambda ab: (mod(ab[0]), mod(ab[1])), [
            (x + 2 * y, y),
            (x - 2 * y, y),
            (x + (2 * y + 1), y),
            (x - (2 * y + 1), y),
            (x, y + 2 * x),
            (x, y - 2 * x),
            (x, y + (2 * x + 1)),
            (x, y - (2 * x + 1)),
        ]))

    def build_graph(self) -> None:
        for x in range(self.size):
            for y in range(self.size):
                for edge in self.edges(x, y):
                    self.graph.elements[self.pair_to_number(x, y)][self.pair_to_number(*edge)] += 1

    def pair_to_number(self, x: int, y: int) -> int:
        return self.size * x + y

    def number_to_pair(self, n: int) -> Tuple[int, int]:
        return n // self.size, n % self.size


class ExpanderZp(AbstractExpander):
    def __init__(self, p: int) -> None:
        self.p: int = p
        super(ExpanderZp, self).__init__(Matrix([[0] * (self.p + 1) for _ in range(self.p + 1)]), 3, 10**-5)

    def get_inverse(self, x: int):
        if x == self.p:
            return 0
        elif x == 0:
            return self.p
        else:
            for inv in range(self.p):
                if (x * inv) % self.p == 1:
                    return inv

    def edges(self, x: int) -> List[int]:
        if x == self.p:
            return [self.p, self.p, self.get_inverse(x)]
        else:
            return [(x + 1) % self.p, (x - 1) % self.p, self.get_inverse(x)]

    def build_graph(self) -> None:
        for x in range(self.p + 1):
            for end_edge in self.edges(x):
                self.graph.elements[x][end_edge] += 1


def run_zn_zn() -> float:
    alphas: List[float] = []
    for n in range(2, 4):
        alphas.append(ExpanderZnZn(n).run())
    return max(alphas)


def run_zp() -> float:
    def is_prime(p: int) -> bool:
        return all(p % i != 0 for i in range(2, int(sqrt(p)+1)))

    alphas: List[float] = []
    for n in filter(is_prime, range(2, 20)):
        alphas.append(ExpanderZp(n).run())
    return max(alphas)


if __name__ == '__main__':
    print('ZnxZn: {}'.format(run_zn_zn()))
    print('Zp: {}'.format(run_zp()))
