from typing import Tuple

from src.matrix import Matrix, Vector, BlockMatrix, IdentityMatrix, HouseholderTransformationMatrix


class ThreeDiagonalization(object):
    """
    Usage:
    $> ex_matrix: Matrix = Matrix([
        [1,  2,  3,  4,  5,  6],
        [2,  8,  9,  10, 11, 12],
        [3, 9, 15, 16, 17, 18],
        [4, 10, 16, 22, 23, 24],
        [5, 11, 17, 23, 29, 30],
        [6, 12, 18, 24, 30, 36],
    ])
    $> ex_three_diagonals, ex_matrix_q = ThreeDiagonalization(ex_matrix).run()
    """
    def __init__(self, matrix: Matrix) -> None:
        if not matrix.is_symmetric:
            raise ValueError("It is not a symmetric matrix")
        self.matrix: Matrix = matrix
        self.size = self.matrix.width

    def run(self) -> Tuple[Matrix, Matrix]:
        matrix_q: Matrix = IdentityMatrix(self.size)
        for i in range(self.size - 2):
            submatrix_size: int = self.size - i - 1
            ui: Vector = Vector([line[i] for line in self.matrix.elements[i+1:]])
            ei: Vector = Vector([1] + [0] * (submatrix_size - 1))
            if ui.to_normalized() == ei:
                continue
            u: Vector = (ui.to_normalized() - ei).to_normalized()
            hh_matrix_u: HouseholderTransformationMatrix = HouseholderTransformationMatrix(u)
            hh_matrix: BlockMatrix = BlockMatrix(
                (
                    (IdentityMatrix(i + 1), Matrix([[0] * submatrix_size for _ in range(i+1)])),
                    (Matrix([[0] * (i + 1) for _ in range(submatrix_size)]), hh_matrix_u)
                )
            )
            self.matrix = ((hh_matrix * self.matrix.to_block_matrix(i+1, i+1)) * hh_matrix).to_matrix()
            matrix_q = (hh_matrix * matrix_q.to_block_matrix(i+1, i+1)).to_matrix()
        return self.matrix, matrix_q


if __name__ == '__main__':
    ex_matrix: Matrix = Matrix([
        [1,  2,  3,  4,  5,  6],
        [2,  8,  9,  10, 11, 12],
        [3, 9, 15, 16, 17, 18],
        [4, 10, 16, 22, 23, 24],
        [5, 11, 17, 23, 29, 30],
        [6, 12, 18, 24, 30, 36],
    ])
    ex_three_diagonals, ex_matrix_q = ThreeDiagonalization(ex_matrix).run()
    print('Diagonalized: \n{}'.format(ex_three_diagonals))
    print('Q: \n{}'.format(ex_matrix_q))
