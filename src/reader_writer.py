from typing import List, Tuple

from src.matrix import Matrix, Vector, Number


class Reader(object):
    @staticmethod
    def read_matrix(message: str = 'Введите матрицу построчно, разделяя числа пробелами') -> Matrix:
        print(message)
        first_line: List[Number] = list(map(float, input().split()))
        return Matrix(
            [first_line] +
            [
                list(map(float, input().split()))
                for _ in range(len(first_line) - 1)
            ]
        )

    @staticmethod
    def read_vector(message: str = 'Введите координаты вектора через пробел') -> Vector:
        print(message)
        return Vector(list(map(float, input().split())))

    @staticmethod
    def read_float(message: str = 'Введите точность') -> float:
        print(message)
        return float(input())

    @staticmethod
    def read_pair_float(message: str = 'Введите пару чисел через пробел') -> Tuple[float, ...]:
        print(message)
        return tuple(map(float, input()))

    @staticmethod
    def read_int(message: str = 'Введите целое число') -> int:
        print(message)
        return int(input())

    @staticmethod
    def read_pair_int(message: str = 'Введите пару целых чисел через пробел') -> Tuple[int, ...]:
        print(message)
        return tuple(map(int, input()))


class Writer(object):
    @staticmethod
    def write_matrix(matrix: Matrix) -> None:
        if isinstance(matrix, Matrix):
            print('\n'.join(' '.join(map(str, line)) for line in matrix.elements))
        else:
            print(0)

    @staticmethod
    def write_vector(vector: Vector) -> None:
        if isinstance(vector, Vector):
            print(' '.join(map(lambda line: str(line[0]), vector.elements)))
        else:
            print(0)
