from math import isclose
from typing import Tuple, List

from src.gershgorin_circles import GershgorinCircles
from src.matrix import Matrix, Number, IdentityMatrix
from src.qr_givens import QRDecompositionGivens, GivensRotationMatrix


class QRThreeDiagonalsDecompositionGivens(QRDecompositionGivens):
    def __init__(self, matrix: Matrix) -> None:
        super().__init__(matrix)

    def run(self) -> Tuple[Matrix, Matrix]:
        for i in range(self.size - 1):
            x: Number = self.matrix.elements[i][i]
            y: Number = self.matrix.elements[i+1][i]
            if not isclose(y, 0, abs_tol=10**-12):
                x1: Number = x / y
                sgn_x: int = 1 if x >= 0 else -1
                s: float = sgn_x / ((1 + x1 ** 2) ** 0.5)
                c: float = s * x1
                givens_matrix: GivensRotationMatrix = GivensRotationMatrix(self.matrix.height, i, i + 1, s, c)
                self.matrix = givens_matrix * self.matrix
                self.qk_transposed = givens_matrix * self.qk_transposed
            elif isclose(x, 0, abs_tol=10**-12):
                givens_matrix: GivensRotationMatrix = GivensRotationMatrix(self.matrix.height, i, i + 1, 0, 1)
                self.matrix = givens_matrix * self.matrix
                self.qk_transposed = givens_matrix * self.qk_transposed

        return self.qk_transposed.transpose(), self.matrix


class ThreeDiagonalsEigenvalues(object):
    """
    Usage:
    $> ex_matrix: Matrix = Matrix([
        [1, 1, 0, 0, 0],
        [1, 2, 1, 0, 0],
        [0, 1, 1, 3, 0],
        [0, 0, 3, 3, 4],
        [0, 0, 0, 4, 2]
    ])
    $> ex_epsilon: float = 10**-7
    $> ex_eigenvalues, ex_eigenvectors = ThreeDiagonalsEigenvalues(ex_matrix, ex_epsilon).run()
    """
    def __init__(self, matrix: Matrix, epsilon: float) -> None:
        self.matrix = matrix
        self.epsilon = epsilon

    def run(self) -> Tuple[List[Number], Matrix]:
        qk: Matrix = IdentityMatrix(self.matrix.width)
        while not all(
                circle.radius < self.epsilon
                for circle in GershgorinCircles.calculate_circles(self.matrix)):
            q_matrix, r_matrix = QRThreeDiagonalsDecompositionGivens(self.matrix).run()
            qk *= q_matrix
            self.matrix = r_matrix * q_matrix
        return self.matrix.diagonal_elements, qk


if __name__ == '__main__':
    ex_matrix: Matrix = Matrix([
        [1, 1, 0, 0, 0],
        [1, 2, 1, 0, 0],
        [0, 1, 1, 3, 0],
        [0, 0, 3, 3, 4],
        [0, 0, 0, 4, 2]
    ])
    ex_epsilon: float = 10**-7
    ex_eigenvalues, ex_eigenvectors = ThreeDiagonalsEigenvalues(ex_matrix, ex_epsilon).run()
    print('Eigenvalues:\n{}'.format(ex_eigenvalues))
    print('Eigenvectors:\n{}'.format(ex_eigenvectors))
