from random import random
from typing import Union, Tuple

from src.matrix import Matrix, Vector, Number


class SimpleIterationMaxEigenvalue(object):
    """
    Usage:
    $> matrix: Matrix = Matrix([[4, 1], [2, -1]])
    $> vector: Vector = SimpleIterationMaxEigenvalue.generate_start_vector(matrix.width)
    $> epsilon: float = 10**-7
    $> result = SimpleIterationMaxEigenvalue(matrix, epsilon).run(vector)
    """
    def __init__(self, matrix: Matrix, epsilon: float) -> None:
        self.matrix = matrix
        self.epsilon = epsilon

    def step(self, x: Vector) -> Vector:
        product: Vector = (self.matrix * x).to_vector()
        return product * (1 / abs(product))

    def check(self, x: Vector, eigenvalue: Number) -> bool:
        return abs((self.matrix * x).to_vector() + x * (-eigenvalue)) < self.epsilon

    @staticmethod
    def generate_start_vector(size: int) -> Vector:
        vector: Vector = Vector([random()] * size)
        return vector * (1 / abs(vector))

    def run(self, vector: Vector, max_count_steps: int = 10**6) -> Union[Number, Tuple[Number, Vector]]:
        for _ in range(max_count_steps):
            vector = self.step(vector)
            eigenvalue: Number = (vector.transpose() * self.matrix * vector).elements[0][0]
            if self.check(vector, eigenvalue):
                return eigenvalue, vector
        return 0


if __name__ == '__main__':
    ex_matrix: Matrix = Matrix([[4, 1], [2, -1]])
    ex_vector: Vector = SimpleIterationMaxEigenvalue.generate_start_vector(ex_matrix.width)
    ex_epsilon: float = 10**-8
    result = SimpleIterationMaxEigenvalue(ex_matrix, ex_epsilon).run(ex_vector)
    print(result)
