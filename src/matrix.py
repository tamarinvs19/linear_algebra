from __future__ import annotations

import itertools
import math
import pprint
from typing import List, Union, Tuple

Number = Union[int, float, complex]


class Matrix(object):
    def __init__(self, elements: List[List[Number]]) -> None:
        if not elements:
            self.height: int = 0
            self.width: int = 0
        else:
            assert all(len(line) == len(elements[0]) for line in elements)
            self.height: int = len(elements)
            self.width: int = len(elements[0])

        self.elements: List[List[Number]] = elements

    @property
    def diagonal_elements(self) -> List[Number]:
        return [self.elements[i][i] for i in range(min(self.width, self.height))]

    @property
    def is_symmetric(self) -> bool:
        return self.width == self.height and all(
            self.elements[i][j] == self.elements[j][i]
            for i, j in itertools.combinations(range(self.width), 2)
        )

    @property
    def is_symmetric_approx(self) -> bool:
        return self.width == self.height and all(
            math.isclose(self.elements[i][j], self.elements[j][i], abs_tol=10**-8)
            for i, j in itertools.combinations(range(self.width), 2)
        )

    def to_vectors(self) -> List[Vector]:
        return [
            Vector([line[i] for line in self.elements])
            for i in range(self.width)
        ]

    def to_vector(self) -> Vector:
        if self.width == 1:
            return Vector([line[0] for line in self.elements])
        else:
            raise TypeError("It is not a vector")

    def to_block_matrix(self, x_sep: int, y_sep: int) -> BlockMatrix:
        a: Matrix = Matrix([line[:y_sep] for line in self.elements[:x_sep]])
        b: Matrix = Matrix([line[y_sep:] for line in self.elements[:x_sep]])
        c: Matrix = Matrix([line[:y_sep] for line in self.elements[x_sep:]])
        d: Matrix = Matrix([line[y_sep:] for line in self.elements[x_sep:]])

        return BlockMatrix(((a, b), (c, d)))

    def transpose(self) -> Matrix:
        return Matrix([
            [self.elements[j][i] for j in range(self.height)]
            for i in range(self.width)
        ])

    def __eq__(self, other) -> bool:
        return self.elements == other.elements

    def __str__(self) -> str:
        return pprint.pformat(
            [list(map(lambda x: round(x, 5), line)) for line in self.elements],
            width=max(list(map(lambda x: len(str(x)), self.elements))) + 2
        )

    def __repr__(self) -> str:
        return 'Matrix({})'.format(self.elements)

    def __add__(self, other: Matrix) -> Matrix:
        if self.width == other.width and self.height == other.height:
            return Matrix(
                [
                    [sum(pair) for pair in zip(self_line, other_line)]
                    for (self_line, other_line) in zip(self.elements, other.elements)
                ]
            )
        else:
            ValueError("Matrices have a different sizes")

    def __mul__(self, other: Union[Matrix, HouseholderTransformationMatrix, Number]) -> Matrix:
        if isinstance(other, HouseholderTransformationMatrix):
            return HouseholderTransformationMatrix.__rmul__(other, self)
        elif isinstance(other, Matrix):
            if self.width == other.height:
                return Matrix(
                    [
                        [
                            sum(self.elements[i][k] * other.elements[k][j] for k in range(self.width))
                            for j in range(other.width)
                        ]
                        for i in range(self.height)
                    ]
                )
            else:
                raise ValueError("Matrixes have a different sizes")
        elif isinstance(other, int) or isinstance(other, float) or isinstance(other, complex):
            return Matrix(
                [
                    [element * other for element in line]
                    for line in self.elements
                ]
            )
        else:
            raise TypeError("It is not a Matrix or Number")


class IdentityMatrix(Matrix):
    def __init__(self, size: int):
        super().__init__([[0] * i + [1] + [0] * (size - i - 1) for i in range(size)])
        self.size: int = size


class Vector(Matrix):
    def __init__(self, elements: List[Number]) -> None:
        super().__init__([[element] for element in elements])
        self.size: int = len(self.elements)

    def to_list(self) -> List[Number]:
        return [line[0] for line in self.elements]

    def to_normalized(self) -> Vector:
        return self * (1 / abs(self))

    def __abs__(self) -> float:
        if self.size == 1:
            return abs(self.elements[0][0])
        else:
            elements: List[Number] = [line[0] for line in self.elements]
            return math.hypot(*map(abs, elements))

    def __eq__(self, other) -> bool:
        return self.elements == other.elements

    def __str__(self) -> str:
        return "({})^T".format(", ".join(map(
            lambda line: str(line[0]),
            self.elements
        )))

    def __repr__(self) -> str:
        return 'Vector({})'.format(self.elements)

    def __add__(self, other: Vector) -> Vector:
        if self.size == other.size:
            return Vector([first[0] + second[0] for (first, second) in zip(self.elements, other.elements)])
        else:
            raise ValueError("Vectors have a different sizes")

    def __sub__(self, other: Vector) -> Vector:
        return self + (other * (-1))

    def __mul__(self, other: Union[Number, Matrix]) -> Union[Vector, Matrix]:
        if isinstance(other, Matrix):
            return Matrix(self.elements) * other
        else:
            return Vector([line[0] * other for line in self.elements])


class BlockMatrix(object):
    """
    Блочная матрица 2x2
    |A B|
    |C D|
    Конструируется из четырех матриц, записанных в ((A, B), (C, D))
    """
    BlockMatrixType = Tuple[Tuple[Matrix, Matrix], Tuple[Matrix, Matrix]]

    def __init__(self, matrixes: BlockMatrixType) -> None:
        self.A = matrixes[0][0]
        self.B = matrixes[0][1]
        self.C = matrixes[1][0]
        self.D = matrixes[1][1]

    def to_matrix(self) -> Matrix:
        return Matrix(
            [a + b for a, b in zip(self.A.elements, self.B.elements)] +
            [c + d for c, d in zip(self.C.elements, self.D.elements)]
        )

    def __repr__(self) -> str:
        return \
            '''BlockMatrix(
            A: {},
            B: {},
            C: {},
            D: {}
            )
            '''.format(self.A, self.B, self.C, self.D)

    def __mul__(self, other: BlockMatrix) -> BlockMatrix:
        return BlockMatrix(
            (
                (self.A * other.A + self.B * other.C,
                 self.A * other.B + self.B * other.D),
                (self.C * other.A + self.D * other.C,
                 self.C * other.B + self.D * other.D)
            )
        )


class HouseholderTransformationMatrix(object):
    """Матрица отражения Хаусхолдера. Умножение происходит без непосредственного умножения матриц"""
    def __init__(self, vector: Vector) -> None:
        self.vector = vector
        self.size = self.vector.size

    def __repr__(self) -> str:
        return 'HouseholderTransformationMatrix: {}'.format(str(self.vector))

    def __mul__(self, other: Matrix) -> Matrix:
        return other + self.vector * (self.vector.transpose() * other) * (-2.0)

    def __rmul__(self, other: Matrix) -> Matrix:
        return other + (other * self.vector) * self.vector.transpose() * (-2.0)
