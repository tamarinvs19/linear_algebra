from typing import Union

from src.gershgorin_circles import GershgorinCircles
from src.matrix import Matrix, Vector, IdentityMatrix


def generate_start_vector(n: int) -> Vector:
    """Возвращает вектор, состоящий из n единиц. Может использоваться для начального вектора"""
    return Vector([1] * n)


class SimpleIterationMethod(object):
    """
    Использование метода простой итерации:
    $> A: Matrix = Matrix([[0.9, 0.3], [0.3, 0.8]])
    $> b: Vector = Vector([0.43, 0.52])
    $> epsilon: float = 0.0001
    $> x0: Vector = Vector([1, 1])
    $> SimpleIterationMethod(A, b, epsilon).run(x0)
    """
    def __init__(self, matrix_a: Matrix, b: Vector, epsilon: float) -> None:
        self.A: Matrix = matrix_a
        self.b: Vector = b
        self.matrix: Matrix = IdentityMatrix(self.b.size) + self.A * (-1)
        self.epsilon: float = epsilon

    def step(self, x: Vector) -> Vector:
        return (self.matrix * x).to_vector() + self.b

    def check_abs(self, x: Vector) -> bool:
        return abs(x - (self.matrix * x).to_vector() - self.b) < self.epsilon

    def run(self, x: Vector) -> Union[Vector, int]:
        bad_steps: int = 0
        check_circles: bool = \
            GershgorinCircles.contain_in_unit_circle(GershgorinCircles.calculate_circles(self.matrix))
        while bad_steps < 20 and check_circles:
            x, old_x = self.step(x), x
            if self.check_abs(x):
                return x
            if abs(x) >= abs(old_x) + 1:
                bad_steps += 1
            else:
                bad_steps = 0
        return 0


class GaussSeidels(object):
    """
    Usage:
    $> A: Matrix = Matrix([[0.9, 0.3], [0.3, 0.8]])
    $> b: Vector = Vector([0.43, 0.52])
    $> epsilon: float = 0.0001
    $> x0: Vector = Vector([1, 1])
    $> GaussSeidels(A, b, epsilon).run(x0)
    """
    def __init__(self, matrix_a: Matrix, b: Vector, epsilon: float) -> None:
        self.A: Matrix = matrix_a
        self.b: Vector = b
        self.L: Matrix = Matrix(
            [
                [element if i >= j else 0 for (j, element) in enumerate(line)]
                for (i, line) in enumerate(self.A.elements)
            ]
        )
        self.U: Matrix = Matrix(
            [
                [element if i < j else 0 for (j, element) in enumerate(line)]
                for (i, line) in enumerate(self.A.elements)
            ]
        )
        self.epsilon: float = epsilon

    @property
    def check_nonzero_diagonal(self) -> bool:
        return all(self.A.elements[i][i] != 0 for i in range(self.A.width))

    def step(self, x: Vector) -> Vector:
        right_vector: Vector = (self.U * (-1) * x).to_vector() + self.b
        result_vector: Vector = Vector([0] * x.size)
        for i in range(x.size):
            result_vector.elements[i][0] = \
                (right_vector.elements[i][0] -
                 sum(result_vector.elements[0][j] * self.L.elements[i][j] for j in range(i)))\
                / self.L.elements[i][i]
        return result_vector

    def check_abs(self, x: Vector) -> bool:
        return abs((self.A * x).to_vector() - self.b) < self.epsilon

    def run(self, x: Vector) -> Union[Vector, int]:
        bad_steps: int = 0
        if not self.check_nonzero_diagonal:
            return 0
        while bad_steps < 20:
            x, old_x = self.step(x), x
            if self.check_abs(x):
                return x
            if abs(x) >= abs(old_x) + 1:
                bad_steps += 1
            else:
                bad_steps = 0
        return 0


if __name__ == '__main__':
    ex_A: Matrix = Matrix([[0.9, 0.3], [0.3, 0.8]])
    ex_b: Vector = Vector([0.43, 0.52])
    ex_epsilon: float = 0.00001
    ex_x0: Vector = Vector([1, 1])
    # or ex_x0: Vector = generate_start_vector(2)
    solutionSIM: Vector = SimpleIterationMethod(ex_A, ex_b, ex_epsilon).run(ex_x0)
    solutionGS: Vector = GaussSeidels(ex_A, ex_b, ex_epsilon).run(ex_x0)
    print('Solution SimpleIteration: {}'.format(solutionSIM))
    print('Solution GaussSeidels: {}'.format(solutionGS))
