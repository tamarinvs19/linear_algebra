from typing import Tuple, List

from src.gershgorin_circles import GershgorinCircles
from src.matrix import Matrix, IdentityMatrix, Number
from src.qr_householder import QRDecompositionHouseholder


class QREigenvalues(object):
    """
    Usage:
    $> ex_matrix: Matrix = Matrix([[0, 1, 1], [1, 0, 1], [1, 1, 0]])
    $> ex_epsilon: float = 10**-7
    $> ex_eigenvalues, ex_eigenvectors = QREigenvalues(ex_matrix, ex_epsilon).run()
    """
    def __init__(self, matrix: Matrix, epsilon: float, approx: bool = False) -> None:
        if approx and not matrix.is_symmetric_approx:
            raise ValueError("It is not a symmetric matrix")
        elif not approx and not matrix.is_symmetric:
            raise ValueError("It is not a symmetric matrix")
        self.matrix: Matrix = matrix
        self.epsilon: float = epsilon

    def run(self) -> Tuple[List[Number], Matrix]:
        qk: Matrix = IdentityMatrix(self.matrix.width)
        while not all(
                circle.radius < self.epsilon
                for circle in GershgorinCircles.calculate_circles(self.matrix)):
            q_matrix, r_matrix = QRDecompositionHouseholder(self.matrix).run()
            qk *= q_matrix
            self.matrix = r_matrix * q_matrix
        return self.matrix.diagonal_elements, qk


if __name__ == '__main__':
    ex_matrix: Matrix = Matrix([
        [1, 1, 0, 0, 2],
        [1, 2, 1, 9, 0],
        [0, 1, 1, 3, 0],
        [0, 9, 3, 3, 4],
        [2, 0, 0, 4, 2]
    ])
    ex_epsilon: float = 10**-4
    ex_eigenvalues, ex_eigenvectors = QREigenvalues(ex_matrix, ex_epsilon).run()
    print('Eigenvalues:\n{}'.format(ex_eigenvalues))
    print('Eigenvectors:\n{}'.format(ex_eigenvectors))
